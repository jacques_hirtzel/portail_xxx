<?php
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";
$aut = "USR_INT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");
require_once(WAY . "/includes/head.inc.php");
?>


<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            <h3>index.php</h3>
        </div>

        <div class="panel-body">
            <table class="table">
            <?php
            echo "<tr>";
                echo "<td><b>";
                    echo "Votre site se trouve à l'emplacement :";
                echo "</b></td>";
                echo "<td>";
                    echo WAY;
                echo "</td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td><b>";
                    echo "L'URL du site est :";
                echo "</b></td>";
                echo "<td>";
                    echo URL;
                echo "</td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td><b>";
                    echo "L'hôte de la base de données est :";
                echo "</td>";
                echo "</b><td>";
                    echo SQL_HOST;
                echo "</td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td><b>";
                    echo "Le Nom d'utilisateur de l'utilisateur de la base de données est :";
                echo "</td>";
                echo "</b><td>";
                    echo SQL_USER;
                echo "</td>";
            echo "</tr>";
            echo "<tr>";
                echo "<td><b>";
                    echo "L'Adresse Email pour la récupération de mot de passe est :";
                echo "</td>";
                echo "</b><td>";
                    echo MAIL_RECOVERY;
                echo "</td>";
            echo "</tr>";
            ?>
            </table>
        </div>

        <div class="panel-footer">

        </div>

    </div>
</div>


