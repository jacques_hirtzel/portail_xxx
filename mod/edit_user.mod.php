<?php
$per = new Personne($_SESSION['id']);
?>
<!-- Modal -->
<div class="modal fade" id="user_change_mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<input type="hidden" name="id_res" id="id_res" value="">
    <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modification du compte</h4>
        </div>

            <div class="modal-body">

                <form class="form-horizontal" id="test_form" method="post" autocomplete="off">
                <div class="form-group row">
                    <label for="nom_ins" class="col-sm-2 col-form-label">Nom</label>
                    <div class="col-sm-10">
                        <input disabled="true" type="text" class="form-control"  placeholder="<?=$per->get_nom()?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="prenom_ins" class="col-sm-2 col-form-label">Prénom</label>
                    <div class="col-sm-10">
                        <div>
                            <input disabled="true" type="text" class="form-control"  placeholder="<?=$per->get_prenom()?>">
                        </div>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="tel_edit" class="col-sm-2 col-form-label">Téléphone</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="tel_edit" name="tel_edit" value="<?=$per->get_tel()?>" placeholder="votre téléphone">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email_edit" class="col-sm-2 col-form-label">E-mail</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="email_edit" name="email_edit" value="<?=$per->get_email()?>" placeholder="votre mail">
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-sm-2 col-form-label" for="old_password_edit">Ancien Mot de passe</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="old_password_edit" name="old_password_edit"  placeholder="votre ancien mot de passe">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label" for="new_password_edit">Nouveau Mot de passe</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="new_password_edit" name="new_password_edit"  placeholder="votre nouveau mot de passe">
                    </div>
                </div>



                <div class="form-group row">
                    <label class="col-sm-2 col-form-label" for="new_password_edit_conf">Conf. Nouveau Mot de passe</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="new_password_edit_conf" name="new_password_edit_conf" placeholder="votre nouveau mot de passe">
                    </div>
                </div>
            </div>
          <div class="modal-footer">
              <div class="col-md-offset-8 col-md-2">
                  <input type="submit" class="btn btn-primary" id="btn_add_ins" value="Modifier">
              </div>

              <div class="col-md-2">
                  <input type="reset" class="form-control btn btn-warning" id="reset_conf" value="Annuler">
              </div>
              </form>
          </div>
        </div>
    </div>
</div>
<script src= "<?=URL?>/js/edit_user.js"></script>

