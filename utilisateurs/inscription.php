<?php

require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";
$aut = "ADM_USR";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");

?>
        
<div class="row">
    <div class="header">
        <h1></h1>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            Inscription au portail
        </div>

        <div class="panel-body">
            <form id="inscription_form">

                <!-- Nom -->
                <div class="form-group row">
                    <label for="nom_per" class="col-sm-2 col-form-label">Nom</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nom_per" name="nom_per" placeholder="Votre nom">
                    </div>
                </div>

                <!-- Prénom -->
                <div class="form-group row">
                    <label for="prenom_per" class="col-sm-2 col-form-label">Prénom</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="prenom_per" name="prenom_per" placeholder="Votre nom">
                    </div>
                </div>


                <!-- Email -->
                <div class="form-group row">
                    <label for="tel_per" class="col-sm-2 col-form-label">Téléphone</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="tel_per" name="tel_per" placeholder="Votre téléphone">
                    </div>
                </div>

                <!-- Email -->
                <div class="form-group row">
                    <label for="email_per" class="col-sm-2 col-form-label">E-mail</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="email_per" name="email_per" placeholder="Votre adresse e-mail">
                    </div>
                </div>

                <!-- Mot de passe -->
                <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">Mot de passe</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Votre mot de passe">
                    </div>
                </div>

                <!-- Confirmation du mot de passe -->
                <div class="form-group row">
                    <label for="password_conf" class="col-sm-2 col-form-label">Confirmation du mot de passe</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="password_conf" name="password_conf" placeholder="La confirmation de votre mot de passe">
                    </div>
                </div>

                <!-- Groupe de boutton -->
                <div class="form-group action-button">
                    <input type="submit" class="btn btn-primary" value="Inscrire">
                    <a href="../index.php"><button type="button" class="btn btn-warning">Annuler</button></a>
                </div>

            </form>
        </div>

        <div class="panel-footer">

        </div>

    </div>

</div>
<script src="<?=WAY?>js/function.js"></script>
<script src="<?=URL?>/utilisateurs/js/inscription.js"></script>
