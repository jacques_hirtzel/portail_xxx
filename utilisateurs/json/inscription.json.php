<?php
header('Content-Type: application/json');
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");

$per = new Personne();

if($per->check_email($_POST['email_per'])) {

    $tab['reponse'] = false;
    $tab['message']['texte'] = "cet email est déjà utilisé !";
    $tab['message']['type'] = "danger";

}else {

    $id = $per->add($_POST);
    $per->set_id($id);

    if($per->init()) {

        $tab['response'] = true;
        $tab['message']['texte'] = "Bienvenue, utilisez les identifiants créés pour vous connecter ! <br><a href=\"/index.php\">Acceuil</a>";
        $tab['message']['type'] = "success";
    }

}

echo json_encode($tab);
?>