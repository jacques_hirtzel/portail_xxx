<?php
/**
 * Created by PhpStorm.
 * User: CP-17JUS
 * Date: 24.09.2019
 * Time: 10:15
 */

session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");

$per = new Personne();

// Regarde si la combinaison email et mot de passe existe
if($_REQUEST['email_edit'] != $_SESSION['email']) {
    if ($per->check_email($_REQUEST['email_edit'])) {
        echo "false";
    } else {
        echo "true";
    }
}else{
    echo "true";
}
