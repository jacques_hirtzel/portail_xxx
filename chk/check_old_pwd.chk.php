<?php
/**
 * Created by PhpStorm.
 * User: CP-17JUS
 * Date: 24.09.2019
 * Time: 10:15
 */

session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";
require_once(WAY . "/includes/autoload.inc.php");

$per = new Personne();

// Regarde si la combinaison email et mot de passe existe
if($per->check_login($_SESSION['email'], $_REQUEST['old_password_edit'])) {
    echo "true";
}else {
    echo "false";
}
