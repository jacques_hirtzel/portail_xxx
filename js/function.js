
//fonction d'affichage des messages
function message(message, type) {

    $("#alert").attr("class", "").addClass("alert");
    $("#alert").addClass("alert-" + type);
    $("#alert .message").html(message);
    $("#alert").css("display", "block");

}