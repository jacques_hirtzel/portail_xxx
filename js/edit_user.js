$(function(){

    $.validator.addMethod("NEWPWCHECK", function(value, element) {

        if(value == ''){
            return true;
        }else {
            return /^(?=.*?[A-Z]+)(?=(.*[a-z])+)(?=(.*[0-9])+)(?=(.*[$@!%*ç#?&\.\-_,;:ÀÁÂÃÆÇÈÉÊËÌÍÎÏÑÒÓÔŒÙÚÛÜÝŸàáâæèéêëìíîïñòóôœùúûüýÿ=()[\]{}])+).{8,}$/.test(value);
        }
    });

    $("#test_form").validate(
        {
            rules:{
                email_edit:{
                    remote: "/portail_xxx/chk/check_new_email.chk.php?_="+Date.now(),
                    required:true,
                    email:true

                },
                tel_edit:{
                    required:true,
                    /*number: true,*/
                    minlength:10

                },
                old_password_edit: {
                    required: true,
                    remote: "/portail_xxx/chk/check_old_pwd.chk.php?_="+Date.now(),
                    //CHECKOLDPW: true
                },
                new_password_edit: {
                    required: false,
                    NEWPWCHECK: true
                },
                new_password_edit_conf: {
                    required: false,
                    equalTo: "#new_password_edit"
                }
            },
            messages: {
                email_edit: {
                    remote:"cette adresse est déjà enregistrée sur ce site",
                    required: "Veuillez saisir votre email",
                    email: "Votre adresse e-mail doit avoir le format suivant : name@domain.com"
                },
                old_password_edit: {
                    required: "Veuillez saisir votre mot de passe",
                    remote: "Votre mot de passe est faux veuillez réessayer"
                },
                new_password_edit: {
                    PWCHECK: "Le mot de passe doit comporter au minimum 8 caractères, dont une minuscule, une majuscule, un chiffre et un caractère spécial"
                },
                new_password_edit_conf: {
                    equalTo: "Le mot de passe n'est pas identique"
                }
            },
            submitHandler: function(form) {

                $.post(
                    "/portail_xxx/json/edit_user.json.php?_="+Date.now(),
                    {
                        new_password: $("#new_password_edit").val(),
                        tel_edit: $("#tel_edit").val(),
                        email_edit: $("#email_edit").val()
                    },
                    function result(data, status){
                        location.replace("/portail_xxx/login.php");
                    }
                );
            }
        }
    );
});