$(function(){

    $("#recovery_form").validate(
        {
            rules:{
                email_per: {
                    required: true,
                    email: true

                }
            },
            messages: {
                email_per: {
                    required: "Veuillez saisir votre email",
                    email: "Votre adresse e-mail doit avoir le format suivant : name@domain.com"
                }
            },
            submitHandler: function(form) {

                $.post(
                    "./json/password_recovery.json.php?_="+Date.now(),
                    {
                        email_per: $("#email_per").val(),
                    },
                    function result(data, status){
                        location.replace("/portail_xxx/login.php");
                    }
                );
            }
        }
    );
});
