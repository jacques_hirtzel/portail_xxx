$(function(){

    $("#login_form").validate(
        {
            rules:{
                email_per: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                email_per: {
                    required: "Veuillez saisir votre email",
                    email: "Votre adresse e-mail doit avoir le format suivant : name@domain.com"
                },
                password: {
                    required: "Veuillez saisir votre mot de passe"
                }
            },
            submitHandler: function(form) {

                $.post(
                    "./json/login.json.php?_="+Date.now(),
                    {
                        email_per: $("#email_per").val(),
                        password: $("#password").val()
                    },
                    function result(data, status){

                        message(data.message.texte, data.message.type);

                        if(data.message.type == "success") {

                            location.replace("./index.php");
                        }
                    }
                );
            }
        }
    );
});
