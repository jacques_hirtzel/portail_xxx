<?php
if(isset($_SESSION['id'])){
    $per = new Personne($_SESSION['id']);
    ?>

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= URL ?>/index.php">portail_xxx</a>
        <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav">

                    <?php
                    if($per->check_aut("ADM_USR")) { ?>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Utilisateurs
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="<?= URL ?>/utilisateurs/inscription.php">Inscription</a></li>
                            <li><a class="dropdown-item" href="<?= URL ?>/droits/attribution_fnc_per.php">Fonctions</a></li>
                            <li><a class="dropdown-item" href="<?= URL ?>/droits/attribution_aut_fnc.php">Droits des fonctions</a></li>
                        </ul>
                    </li>

                    <?php } ?>
                </ul>
                <ul class="navbar-nav nav navbar-right dropdown">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a class="dropdown-item" data-toggle="modal" data-target="#user_change_mod">Mon Profil</a></li>
                            <li><a class="dropdown-item" href="<?= URL?>/logout.php">Déconnexion</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<br>


<?php
    include (WAY."/mod/edit_user.mod.php");
}
?>