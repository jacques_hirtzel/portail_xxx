<?php
session_start();
require_once(WAY . "includes/autoload.inc.php");

if(!isset($_SESSION['id'])) { // Aucune session ouverte

    // Redirige vers le login
    session_destroy();
    header('Location: ' . URL . '/login.php');
    exit;

}else { // Une session potentiellement non conforme est ouverte

    // Redirige vers la page d'accueil pour les pages login et inscription
    if($_SERVER["REQUEST_URI"] === "/login.php") {

        header('Location: ' . URL . '/index.php');
        exit;
    }

    // Redirige vers le login si la personne n'est pas correctement connecté
    $per = new Personne($_SESSION['id']);
    if(!$per->check_connect()){
        session_destroy();
        header('Location: ' . URL . '/login.php');
        exit;
    }
}

if(!$per->check_aut($aut)) {
    header('Location: ' . URL . "/index.php");
    exit;
}

?>