<?php

// Inclusion de la class correspondante au paramètre passé.
function chargerClasse($classe) {
    require WAY . 'class/' . $classe . '.class.php';
}

// Enrengistre charcherClasse en autoload
spl_autoload_register('chargerClasse');
