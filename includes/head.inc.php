<?php
header('Cache-Control: no-cache');
header('Pragma: no-cache');
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" Content="no-cache">
    <meta http-equiv="Pragma" Content="no-cache">
    <meta http-equiv="Cache" Content="no store">
    <meta http-equiv="Expires" Content="0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Portail_xxx</title>

    <!-- CSS utilisée sur tout le site -->
    <link rel="stylesheet" href="<?= URL ?>/css/global.css">
    <link rel="stylesheet" href="css/module.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <!-- Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <!-- Jquery validation plugin -->
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>

    <!-- Fonction JS -->
    <script src="<?= URL ?>/js/function.js"></script>

    <script src="<?= URL ?>/plugins/bootbox/bootbox.min.js"></script>
</head>
<body>
    <div class="container">
        <div id="loading">
            <img id="loading_img" src="<?= URL ?>/icones/loading.gif">
        </div>
        <!-- Zone de notification -->
        <div class="alert" id="alert">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong class="bold"></strong><span class="message"></span>
        </div>
        <?php require_once( WAY."/includes/menu.inc.php")?>