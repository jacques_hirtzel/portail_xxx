<?php
header('Content-Type: application/json');
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";
require_once(WAY . "includes/autoload.inc.php");

$per = new Personne();

// Regarde si la combinaison email et mot de passe existe
if($per->check_login($_POST['email_per'], $_POST['password'])) {

    $tab['reponse'] = true;
    $tab['message']['texte'] = "Connexion valide.";
    $tab['message']['type'] = "success";

}else {

    $tab['response'] = true;
    $tab['message']['texte'] = "Connexion invalide !";
    $tab['message']['type'] = "danger";

}

echo json_encode($tab);

