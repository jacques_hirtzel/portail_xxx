<?php
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');

require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";


require_once(WAY . "/includes/autoload.inc.php");
$per = new Personne();

if($per->check_email($_POST['email_per'])){
    $password = $per->gen_random_password();
    $password_mail = $password;
    $per->recover_password($_POST['email_per'],$password);

    //envoi d'un mail
    $to = $_POST['email_per'];
    $subject = "Recupération du mot de passe";
    $txt =  "Ceci est un message automatique veuillez ne pas répondre\n
Bonjour,\n
Voici votre nouveau mot de passe : ".$password_mail."\n
Pensez à le changer rapidement pour des raisons de sécurité.\n
Salutations et bonne soirée,\n
l'équipe du portail_xxx.";
    $headers =  "From: ".MAIL_RECOVERY. "\r\n"
                ."Content-Type:text;charset=utf-8";

    mail($to,$subject,$txt,$headers);

}
echo json_encode("");
?>
