<?php
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";

$aut = "ADM_RES";
require(WAY . "/includes/secure.inc.php");

$per = new Personne($_SESSION['id']);

$per->edit($_POST);

echo json_encode($per);