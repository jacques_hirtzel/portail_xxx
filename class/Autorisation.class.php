<?php
Class Autorisation EXTENDS Projet{

    // PARAMETERS
    private $id;
    private $nom;
    private $code;
    private $desc;

    /**
     * Constructeur de la fonction
     * @param null $id
     */
    public function __construct($id = null) {

        parent::__construct();

        if($id){
            $this->set_id($id);
            $this->init();
        }

    }

    /**
     * Initialisation de l'objet
     * @return bool
     */
    public function init() {

        $query = "SELECT * FROM t_autorisations WHERE id_aut=:id_aut";

        try {

            $stmt = $this->pdo->prepare($query);
            $args['id_aut'] = $this->get_id();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_nom($tab['nom_aut']);
            $this->set_code($tab['code_aut']);
            $this->set_desc($tab['desc_aut']);
            return true;
        } catch (Exception $e) {

            return false;
        }

        return true;
    }

    /**
     * Ajout d'une autorisation à une fonction
     * @param $id_fnc
     * @return bool
     */
    public function add_aut_fnc($id_fnc) {

        $id_aut = $this->get_id();

        // Tableau d'arguments
        $args['id_aut'] = $id_aut;
        $args['id_fnc'] = $id_fnc;

        // Requête
        $query = "INSERT INTO t_aut_fnc SET "
            . "id_aut = :id_aut, "
            . "id_fnc = :id_fnc";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return true;

        } catch (Exception $e) {

            echo $e;
            return false;
        }

    }

    /**
     * Retrait d'une autorisation à une fonction
     * @param $id_fnc
     * @return bool
     */
    public function del_aut_fnc($id_fnc) {

        $id_aut = $this->get_id();

        // Tableau d'arguments
        $args['id_aut'] = $id_aut;
        $args['id_fnc'] = $id_fnc;

        // Requête
        $query = "DELETE FROM t_aut_fnc WHERE "
            . "id_aut = :id_aut AND "
            . "id_fnc = :id_fnc";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return true;

        } catch (Exception $e) {

            echo $e;
            return false;
        }
    }

    /**
     * Récupère toutes les autorisations
     * @param string $order
     * @return array|bool
     */
    public function get_all($order = "nom_aut") {

        $query = "SELECT * FROM t_autorisations ORDER BY ".$order;

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return($tab);

        } catch(Exception $e) {

            return false;
        }
    }

    /**
     * Récupère toutes les autorisations de toutes les fonctions
     * @return array|bool
     */
    public function get_tab_fnc_all_aut(){
        $query = "SELECT * FROM t_aut_fnc ORDER BY id_aut";

        try {

            $stmt = $this->pdo->prepare($query);
            if($stmt->execute()) {
                $tab = $stmt->fetchAll();
                $tab_aut_fnc = Array();
                foreach($tab as $row) {
                    $tab_aut_fnc[$row['id_aut']][] = $row['id_fnc'];
                }
                return $tab_aut_fnc;
            }else {

                return false;
            }

        }catch(Exception $e) {

            return false;
        }
    }

    /**
     * Fonction de base toString
     * @return string
     */
    public function __toString() {

        $str = "\n<pre>\n";
        foreach($this as $key => $val){
            if($key != "pdo"){
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for($i = $lengh_key; $i < 20;$i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }

    /**
     * Ajout d'une autorisation
     * @param $tab
     * @return bool|string
     */
    public function add($tab) {

        // Tableau d'arguments
        $args['nom_aut'] = $tab['nom_aut'];
        $args['code_aut'] = $tab['code_aut'];
        $args['desc_aut'] = $tab['desc_aut'];

        // Requête
        $query = "INSERT INTO t_autorisations SET "
            . "nom_aut = :nom_aut, "
            . "code_aut = :code_aut, "
            . "desc_aut = :desc_aut";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return $this->pdo->lastInsertId();

        } catch (Exception $e) {

            echo $e;
            return false;
        }
    }

    /**
     * Verifie si un code existe dans la base de données
     * @param $code
     * @return bool
     */
    public function check_code($code){

        $query = "SELECT * FROM t_autorisations WHERE code_aut = :code LIMIT 1";

        try {

            $stmt = $this->pdo->prepare($query);
            $args[':code'] = $code;
            $stmt->execute($args);
            $tab = $stmt->fetch();

            if(strtolower($tab['code_aut']) == strtolower($code)){

                return true;
            }else {

                return false;
            }

        } catch (Exception $e) {

            return false;
        }
    }

    // METHOD: Setter & Getter
    public function set_id($id) {
        $this->id = $id;
    }
    public function get_id() {
        return $this->id;
    }

    public function set_nom($nom) {
        $this->nom = $nom;
    }
    public function get_nom() {
        return $this->nom;
    }

    public function set_code($code) {
        $this->code = $code;
    }
    public function get_code() {
        return $this->code;
    }

    public function set_desc($desc) {
        $this->desc = $desc;
    }
    public function get_desc() {
        return $this->desc;
    }
}
