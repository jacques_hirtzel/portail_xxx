<?php
Class Fonction EXTENDS Projet{

    // PARAMETERS
    private $id;
    private $nom;
    private $abr;
    private $desc;

    // METHOD: Various

    /**
     * Constructeur de la fonction
     * @param null $id
     */
    public function __construct($id = null) {

        parent::__construct();

        if($id){
            $this->set_id($id);
            $this->init();
        }

    }

    /**
     * Initialisation de l'objet
     * @return bool
     */
    public function init() {

        $query = "SELECT * FROM t_fonctions WHERE id_fnc=:id_fnc";

        try {

            $stmt = $this->pdo->prepare($query);
            $args['id_fnc'] = $this->get_id();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_nom($tab['nom_fnc']);
            $this->set_abr($tab['abr_fnc']);
            $this->set_desc($tab['desc_fnc']);
            return true;
        } catch (Exception $e) {

            return false;
        }

        return true;
    }

    /**
     * Récupère toutes les Fonctions
     * @param string $order
     * @return array|bool
     */
    public function get_all($order = "nom_fnc") {

        $args[':order'] = $order;
        $query = "SELECT * FROM t_fonctions ORDER BY :order";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return($tab);

        } catch(Exception $e) {

            return false;
        }
    }

    /**
     * Récupère toutes les foctions de toutes les personnes
     * @return array|bool
     */
    public function get_tab_per_all_fnc(){
        $query = "SELECT * FROM t_fnc_per ORDER BY id_fnc";

        try {

            $stmt = $this->pdo->prepare($query);
            if($stmt->execute()) {
                $tab = $stmt->fetchAll();
                $tab_fnc_per = Array();
                foreach($tab as $row) {
                    $tab_fnc_per[$row['id_fnc']][] = $row['id_per'];
                }
                return $tab_fnc_per;
            }else {

                return false;
            }

        }catch(Exception $e) {

            return false;
        }
    }

    /**
     * fonction de base toString
     * @return string
     */
    public function __toString() {

        $str = "\n<pre>\n";
        foreach($this as $key => $val){
            if($key != "pdo"){
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for($i = $lengh_key; $i < 20;$i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }

    /**
     * Ajout d'une fonction dans la base
     * @param $tab
     * @return bool|string
     */
    public function add($tab){

        // Tableau d'arguments
        $args['nom_fnc'] = $tab['nom_fnc'];
        $args['abr_fnc'] = $tab['abr_fnc'];
        $args['desc_fnc'] = $tab['desc_fnc'];

        // Requête
        $query = "INSERT INTO t_fonctions SET "
            . "nom_fnc = :nom_fnc, "
            . "abr_fnc = :abr_fnc, "
            . "desc_fnc = :desc_fnc";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return $this->pdo->lastInsertId();

        } catch (Exception $e) {

            echo $e;
            return false;
        }
    }

    /**
     * Verifie si une abreviation existe dans la base de données
     * @param $abr
     * @return bool
     */
    public function check_abr($abr){

        $query = "SELECT * FROM t_fonctions WHERE abr_fnc = :abr LIMIT 1";

        try {

            $stmt = $this->pdo->prepare($query);
            $args[':abr'] = $abr;
            $stmt->execute($args);
            $tab = $stmt->fetch();

            if(strtolower($tab['abr_fnc']) == strtolower($abr)){

                return true;
            }else {

                return false;
            }

        } catch (Exception $e) {

            return false;
        }
    }

    /**
     * Getter et setter
     * @param $id
     */
    public function set_id($id) {
        $this->id = $id;
    }
    public function get_id() {
        return $this->id;
    }

    public function set_nom($nom) {
        $this->nom = $nom;
    }
    public function get_nom() {
        return $this->nom;
    }

    public function set_abr($abr) {
        $this->abr = $abr;
    }
    public function get_abr() {
        return $this->abr;
    }

    public function set_desc($desc) {
        $this->desc = $desc;
    }
    public function get_desc() {
        return $this->desc;
    }
}
