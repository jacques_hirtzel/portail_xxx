<?php
Class Projet {

    protected $pdo;

    /**
     * Constructeur de la fonction
     * @param string $data_base
     */
    public function __construct($data_base = "") {

        switch ($data_base){
            case "":
                $this->pdo = new PDO('mysql:dbname=' . BASE_NAME . ';host=' . SQL_HOST, SQL_USER, SQL_PASSWORD,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));
            break;

            case "CMSMS":
                $this->pdo = new PDO('mysql:dbname=' . BASE_NAME_CMSMS . ';host=' . SQL_HOST_CMSMS , SQL_USER_CMSMS , SQL_PASSWORD_CMSMS ,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC));
            break;
        }
    }
}
