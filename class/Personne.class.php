<?php
Class Personne EXTENDS Projet{

	// PARAMETERS
    private $id;
    private $nom;
    private $prenom;
    private $email;
    private $tel;
    private $password;

    /**
     * Constructeur de la fonction
     * @param null $id
     */
	public function __construct($id = null) {

	    parent::__construct();

		if($id){
		    $this->set_id($id);
		    $this->init();
        }

	}

    /**
     * Initialisation de l'objet
     * @return bool
     */
	public function init() {

	    $query = "SELECT * FROM t_personnes WHERE id_per=:id_per";
	    try {

	        $stmt = $this->pdo->prepare($query);
	        $args['id_per'] = $this->get_id();
	        $stmt->execute($args);
	        $tab = $stmt->fetch();

	        $this->set_nom($tab['nom_per']);
	        $this->set_prenom($tab['prenom_per']);
	        $this->set_email($tab['email_per']);
	        $this->set_tel($tab['tel_per']);
	        $this->set_password($tab['password_per']);
	        return true;
        } catch (Exception $e) {

	        return false;
        }
        return true;
    }

    /**
     * Fonction de base toString
     * @return string
     */
	public function __toString() {
		
		$str = "\n<pre>\n";
		foreach($this as $key => $val){
		    if($key != "pdo"){
		        $str .= "\t" . $key;
		        $lengh_key = strlen($key);
		        for($i = $lengh_key; $i < 20;$i++) {
		            $str .= "&nbsp;";
                }
		        $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
		$str .= "\n</pre>";
		return $str;
	}

    /**
     * Ajout d'une personne dans la base
     * @param $tab
     * @return bool|string
     */
	public function add($tab)
    {

        $this->gen_password($tab["password"]);

        // Tableau d'arguments
        if (isset($tab['nom_per'])){
            $args['nom_per'] = $tab['nom_per'];
            $args['prenom_per'] = $tab['prenom_per'];
            $args['tel_per'] = $tab['tel_per'];
        }
        $args['email_per'] = $tab['email_per'];
        $args['password_per'] = $this->get_password();

        // Requête
        if (isset($tab['nom_per'])) {
            $query = "INSERT INTO t_personnes SET "
                . "nom_per = :nom_per, "
                . "prenom_per = :prenom_per, "
                . "email_per = :email_per, "
                . "tel_per = :tel_per, "
                . "password_per = :password_per";
        }else{
            $query = "INSERT INTO t_personnes SET "
                . "email_per = :email_per, "
                . "password_per = :password_per";
        }
        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return $this->pdo->lastInsertId();

        } catch (Exception $e) {

            echo $e;
            return false;
        }
    }

    /**
     * Fonction de mot de passe oublié
     * @param $email
     * @param $password
     * @return bool
     */
    public function recover_password($email,$password){
        $this->gen_password($password);

        // Tableau d'arguments
        $args['email_edit'] = $email;
        $args['password_edit'] = $this->get_password();

        // Requête
        $query ="UPDATE t_personnes
                SET password_per = :password_edit
                WHERE email_per = :email_edit";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return true;

        } catch (Exception $e) {

            echo $e;
            return false;
        }
    }

    /**
     * suppression d'une personne
     * @param $id_per
     * @return bool|string
     */
    public function del($id_per){
        $query = "DELETE FROM t_personnes WHERE 
            id_per = :id_per";
        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($id_per);
            return "";

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Verifie si un email existe dans la base de données
     * @param $email
     * @return bool
     */
    public function check_email($email){

	    $query = "SELECT * FROM t_personnes WHERE email_per = :email LIMIT 1";

	    try {

	        $stmt = $this->pdo->prepare($query);
	        $args[':email'] = $email;
	        $stmt->execute($args);
	        $tab = $stmt->fetch();
	        if(strtolower($tab['email_per']) == strtolower($email)){

	            return true;
            }else {

	            return false;
            }

        } catch (Exception $e) {

	        return false;
        }
    }

    /**
     * Vérifie si l'identifiant et le mot de passe correspondent aux informations dans la base
     * @param $email
     * @param $password
     * @return bool
     */
    public function check_login($email, $password){
	    $query = "SELECT id_per, password_per FROM t_personnes WHERE email_per=:email LIMIT 1";
	    try {

	        $stmt = $this->pdo->prepare($query);
	        $args[':email'] = $email;
	        $stmt->execute($args);
	        $tab = $stmt->fetch();
            if(password_verify($password, $tab['password_per'])){

	            $_SESSION['id'] = $tab['id_per'];
	            $user_browser_ip = $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'];
	            $_SESSION['login_string'] = password_hash($tab['password_per'] . $user_browser_ip, PASSWORD_DEFAULT);
	            $_SESSION['email'] = $email;

	            return true;
            }else {

	            return false;
            }

        } catch(Exception $e) {

	        return false;
        }
    }

    /**
     * Vérifie si l'utilisateur est déjà connecté
     * @return bool
     */
    public function check_connect(){

	    if(isset($_SESSION['id'], $_SESSION['email'], $_SESSION['login_string'])) {

	        $user_browser_ip = $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'];

	        if(password_verify($this->get_password() . $user_browser_ip, $_SESSION['login_string'])) {

	            return true;
            }else {

	            return false;
            }

        }else {

	        return false;
        }

    }

    /**
     * Permet la modifiaction des informations de l'utilisateur
     * @param $tab
     * @return bool|string
     */
    public function edit ($tab){
        $args['tel_edit'] = $tab['tel_edit'];
        $args['email_edit'] = $tab['email_edit'];
        $args['id_edit'] = $this->get_id();
        if($tab['new_password'] != null) {
            $this->gen_password($_POST['new_password']);
            $args['password_edit'] =  $this->get_password();
            $query =   "UPDATE t_personnes
                        SET email_per = :email_edit,
                        tel_per = :tel_edit,
                        password_per = :password_edit
                        WHERE id_per = :id_edit";
            try {
                $stmt = $this->pdo->prepare($query);
                $stmt->execute($args);
                return "";

            } catch (Exception $e) {

                return false;
            }
        }else{
            $query =   "UPDATE t_personnes
                        SET email_per = :email_edit,
                        tel_per = :tel_edit
                        WHERE id_per = :id_edit";
            try {
                $stmt = $this->pdo->prepare($query);
                $stmt->execute($args);
                return "";

            } catch (Exception $e) {

                return false;
            }
        }
    }

    /**
     * Récupération de tout les utilisateurs
     * @param string $order
     * @return array|bool
     */
    public function get_all($order = "nom_per, prenom_per") {

        $args[':order'] = $order;
        $query = "SELECT * FROM t_personnes ORDER BY :order";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return($tab);

        } catch(Exception $e) {

            return false;
        }
    }

    /**
     * Récupération de toutes les autorisation des utilisateurs
     * @return array
     */
    public function get_all_aut() {

	    $query = "SELECT code_aut FROM t_aut_fnc AUF "
                ."JOIN t_autorisations AUT ON AUF.id_aut=AUT.id_aut "
                ."JOIN t_fonctions FNC ON AUF.id_fnc=FNC.id_fnc "
                ."JOIN t_fnc_per FNP ON FNC.id_fnc=FNP.id_fnc AND FNP.id_per=:id_per";

	    $args['id_per'] = $this->get_id();

	    $stmt = $this->pdo->prepare($query);
	    $stmt->execute($args);
	    $tab = $stmt->fetchAll();

	    foreach($tab AS $aut) {

	        $tab_aut[] = $aut['code_aut'];
        }

	    return $tab_aut;
    }

    /**
     * Récupération de toutes les fonctions des utilisateurs
     * @return array
     */
    public function get_all_fnc() {

	    $query = "SELECT abr_fnc, nom_fnc FROM t_fonctions FNC "
                ."JOIN t_fnc_per FNP ON FNC.id_fnc=FNP.id_fnc AND FNP.id_per=:id_per ";
	    try {

	        $args['id_per'] = $this->get_id();
	        $stmt = $this->pdo->prepare($query);

	        if($stmt->execute($args)) {

	            $tab_aut = $stmt->fetchAll();
	            return $tab_aut;
            }else {

	            return false;
            }

        } catch (Exception $e) {

	        return false;
        }
    }

    /**
     * Contrôle si l'utilisateur a les autorisations requises
     * @return array
     */
    public function check_aut($aut_list) {

	    $tab_aut = explode(";", $aut_list);
	    $tab_aut_per = $this->get_all_aut();

	    foreach ($tab_aut AS $aut) {
	        if(in_array($aut, $tab_aut_per)) {

	            return true;
            }
        }

	    return false;
    }

    /**
     * Ajout d'une fonction à un utilisateur
     * @param $id_fnc
     * @return bool
     */
    public function add_fnc($id_fnc) {

	    $id_per = $this->get_id();

        // Tableau d'arguments
        $args['id_per'] = $id_per;
        $args['id_fnc'] = $id_fnc;

        // Requête
        $query = "INSERT INTO t_fnc_per SET "
            . "id_per = :id_per, "
            . "id_fnc = :id_fnc";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return true;

        } catch (Exception $e) {

            echo $e;
            return false;
        }

    }

    /**
     * Suppression d'une fonction à un utilisateur
     * @param $id_fnc
     * @return bool
     */
    public function del_fnc($id_fnc) {

        $id_per = $this->get_id();

        // Tableau d'arguments
        $args['id_per'] = $id_per;
        $args['id_fnc'] = $id_fnc;

        // Requête
        $query = "DELETE FROM t_fnc_per WHERE "
            . "id_per = :id_per AND "
            . "id_fnc = :id_fnc";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return true;

        } catch (Exception $e) {
            echo $e;
            return false;
        }

    }

    /**
     * Crypte le mot de passe reçu
     * @param $password
     */
    public function gen_password($password) {
	    $this->set_password(password_hash($password, PASSWORD_DEFAULT));
    }

    /**
     * Crée un mot de passe fort aléatoire
     * @return string
     */
    public function gen_random_password(){
	    $tab_car[0]= "abcdefghijklmnopqrstuvwxyz";
	    $tab_car[1]= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    $tab_car[2]= "0123456789";
	    $tab_car[3]= "!?\$_;-#=()*[]{}";
        $password = array();
        $new_password ="";

	    for ($i = 0;$i <=3; $i++){
	        $index = rand(0,3);
	        $password[] = substr($tab_car[$index], rand(0,strlen($tab_car[$index])-1),1) ;
	        $password[] = substr($tab_car[$i], rand(0,strlen($tab_car[$i])-1),1) ;

        }

        for($j=0;$j <= 7;$j++){
            $index = rand(0,sizeof($password)-1);
            $new_password .= $password[$index];
            $password[$index] = $password[sizeof($password)-1];
            unset($password[sizeof($password)-1]);
        }
        return $new_password;

    }

    // METHOD: Setter & Getter
    public function set_id($id) {
        $this->id = $id;
    }
    public function get_id() {
        return $this->id;
    }

    public function set_nom($nom) {
        $this->nom = $nom;
    }
    public function get_nom() {
        return $this->nom;
    }

    public function set_prenom($prenom) {
        $this->prenom = $prenom;
    }
    public function get_prenom() {
        return $this->prenom;
    }

    public function set_email($email) {
        $this->email = $email;
    }
    public function get_email() {
        return $this->email;
    }

    public function set_password($password) {
        $this->password = $password;
    }
    public function get_password() {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function get_tel()
    {
        return $this->tel;
    }

    /**
     * @param mixed $tel
     */
    public function set_tel($tel)
    {
        $this->tel = $tel;
    }

}
