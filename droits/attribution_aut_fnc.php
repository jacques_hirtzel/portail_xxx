<?php
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";

$aut = "ADM_AUT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
?>

<div class="row">
    <div class="header">
        <h1></h1>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            Attribution des droits aux rôles
        </div>

        <div class="panel-body">

            <?php
            $aut = new Autorisation();
            $tab_aut = $aut->get_all();

            $fnc = new Fonction();
            $tab_fnc = $fnc->get_all();

            $tab_fnc_aut = $aut->get_tab_fnc_all_aut();
            ?>

            <table class="table table-condensed table-bordered">
                <tr>
                    <th>Droits et leur description</th>
                    <?php
                    foreach($tab_fnc as $fonction) {

                        echo "<th class='text-center'>" . $fonction["nom_fnc"] . "</th>";
                    }
                    ?>
                </tr>
                <?php
                
                $tab_aut = $aut->get_all();

                foreach($tab_aut as $autorisation) {
              
                    echo "<tr>";
                    echo "<td><b>";
                    echo substr($autorisation["code_aut"], 0, 3) === "ADM" ? "Administrateur": "Utilisateur";
                    echo " - " . $autorisation["nom_aut"] . "</b>";
                    echo "<br><span>" . $autorisation["desc_aut"]; 
                    echo "</span></td>";

                    foreach($tab_fnc as $fonction){
                        echo "<td class='text-center' >";

                        echo "<input ".
                            (($fonction['id_fnc'] == 1) ? "disabled" : "")." type=\"checkbox\" class=\"auth\" title=\"".$fonction["nom_fnc"]."\"id=\"auth_" . $autorisation['id_aut'] . "-" . $fonction['id_fnc'] . "\" id_fnc=\"" . $fonction['id_fnc'] . "\" id_aut=\"" . $autorisation['id_aut'] . "\"";

                        if(isset($tab_fnc_aut[$autorisation['id_aut']])){
                            if(in_array($fonction['id_fnc'], $tab_fnc_aut[$autorisation['id_aut']])){
                                echo " checked=\"checked\"";
                            }
                        }
                        echo ">";
                        echo "</td>";
                    }
                    echo "</tr>";
                }
                ?>

            </table>
        </div>

        <div class="panel-footer">

        </div>

    </div>
</div>

</div>
<script src="./js/attribution_aut_fnc.js"></script>

</body>

</html>