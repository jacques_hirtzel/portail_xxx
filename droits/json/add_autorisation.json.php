<?php
header('Content-Type: application/json');
session_start();

require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";
$aut = "ADM_AUT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$aut_1 = new Autorisation();
$aut_2 = new Autorisation();

if ($aut_1->check_code($_POST['code_aut'])) {

    $tab['reponse'] = false;
    $tab['message']['texte'] = "Cette autorisation existe déjà dans la base";
    $tab['message']['type'] = "danger";

} else {

    $aut_1_data = array();
    $aut_1_data['nom_aut'] = $_POST['nom_aut'];
    $aut_1_data['desc_aut'] = $_POST['desc_aut_admin'];
    $aut_1_data['code_aut'] = "ADM_" . $_POST['code_aut'];

    $aut_2_data = array();
    $aut_2_data['nom_aut'] = $_POST['nom_aut'];
    $aut_2_data['desc_aut'] = $_POST['desc_aut_user'];
    $aut_2_data['code_aut'] = "USR_" . $_POST['code_aut'];

    $id_1 = $aut_1->add($aut_1_data);
    $aut_1->set_id($id_1);

    $id_2 = $aut_2->add($aut_2_data);
    $aut_2->set_id($id_2);

    if ($aut_1->init() && $aut_2->init()) {

        $code_aut_uppercased = strtoupper($_POST['code_aut']);

        $tab['response'] = true;
        $tab['message']['texte'] = "Les autorisation ADM_" . $code_aut_uppercased . " et USR_" . $code_aut_uppercased . " ont bien été ajouté";
        $tab['message']['type'] = "success";
    }

}

echo json_encode($tab);