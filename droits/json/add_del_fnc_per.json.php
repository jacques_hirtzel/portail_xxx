<?php
header('Content-Type: application/json');
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";
$aut = "ADM_AUT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$id_per = $_POST["id_per"];
$id_fnc = $_POST["id_fnc"];
$status = $_POST["status"];
$id_auth = $_POST["id_auth"];

$user = new Personne($id_per);

sleep(1);

if($status === "true") {

    $tab["reponse"] = $user->add_fnc($id_fnc);
    $tab["operation"] = "add";
}else {

    $tab["reponse"] = $user->del_fnc($id_fnc);
    $tab["operation"] = "del";
}

if($tab["reponse"]){

    $tab["message"]["type"] = "success";

    if($tab["operation"] === "add") {

        $tab["message"]["texte"] = "L'ajout de la fonction a bien été enrengistré";
    }else {

        $tab["message"]["texte"] = "La suppression de la fonction a bien été enrengistré";
    }
}else {

    $tab["message"]["type"] = "danger";

    if($tab["operation"] === "add") {

        $tab["message"]["texte"] = "L'ajout de la fonction n'a pas pu se faire";
    }else {

        $tab["message"]["texte"] = "La suppression de la fonction n'a pas pu se faire";
    }
}
$tab['id_auth'] = $id_auth;
echo json_encode($tab);
