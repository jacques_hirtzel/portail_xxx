<?php
header('Content-Type: application/json');
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";

$aut = "ADM_AUT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$id_aut = $_POST["id_aut"];
$id_fnc = $_POST["id_fnc"];
$status = $_POST["status"];
$id_auth = $_POST["id_auth"];

$autorisation = new Autorisation($id_aut);
$tab["id_auth"] = $id_auth;

sleep(1);

if($status === "true") {

    $tab["reponse"] = $autorisation->add_aut_fnc($id_fnc);
    $tab["operation"] = "add";
}else {

    $tab["reponse"] = $autorisation->del_aut_fnc($id_fnc);
    $tab["operation"] = "del";
}

if($tab["reponse"]){

    $tab["message"]["type"] = "success";

    if($tab["operation"] === "add") {

        $tab["message"]["texte"] = "L'ajout de la fonction a bien été enrengistré";
    }else {

        $tab["message"]["texte"] = "La suppression de la fonction a bien été enrengistré";
    }
}else {

    $tab["message"]["type"] = "danger";

    if($tab["operation"] === "add") {

        $tab["message"]["texte"] = "L'ajout de la fonction n'a pas pu se faire";
    }else {

        $tab["message"]["texte"] = "La suppression de la fonction n'a pas pu se faire";
    }
}

echo json_encode($tab);