<?php
header('Content-Type: application/json');
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";
$aut = "ADM_AUT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$fnc = new Fonction();

if ($fnc->check_abr($_POST['abr_fnc'])) {

    $tab['reponse'] = false;
    $tab['message']['texte'] = "Cette fonction est déjà utilisé dans la base !";
    $tab['message']['type'] = "danger";

} else {

    $id = $fnc->add($_POST);
    $fnc->set_id($id);

    if ($fnc->init()) {

        $abr_fnc_lowercased = strtolower($_POST['abr_fnc']);

        $tab['response'] = true;
        $tab['message']['texte'] = "La fonction " . $_POST['nom_fnc'] . " (" . $abr_fnc_lowercased . ") à bien été ajoutée";
        $tab['message']['type'] = "success";
    }

}

echo json_encode($tab);
