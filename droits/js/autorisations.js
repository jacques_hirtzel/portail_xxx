$(function(){
    
	$.validator.addMethod("CDCHECK", function(value, element) {
        
        return /[A-Z]{3}/.test(value);
    });

    $("#autorisations_form").validate(
        {
            rules:{
                nom_aut: {
                    required: true,
                    minlength: 3
                },
                code_aut: {
                    required: true,
                    CDCHECK: true
                },
                desc_aut_admin: {
                    required: true,
                    minlength: 10
                },
                desc_aut_user: {
                    required: true,
                    minlength: 10
                }
            }, 
            messages: {
                nom_aut: {
                    required: "Veuillez saisir le nom de l'autorisation",
                    minlength: "Le nom de l'autorisation doit comporter 3 characters au minimum"
                },
                code_aut: {
                    required: "Veuillez saisir le code de l'autorisation",
                    CDCHECK: "Le code de l'autorisation doit être composé de trois characters en majuscule"
                },
                desc_aut_admin: {
                    required: "Veuillez saisir la description de l'autorisation administrateur",
                    minlength: "La description de l'autorisation administrateur doit faire 10 characters au minimum."
                },
                desc_aut_user: {
                    required: "Veuillez saisir la description de l'autorisation utilisateur",
                    minlength: "La description de l'autorisation utilisateur doit faire 10 characters au minimum."
                }
            },
            submitHandler: function(form) {

                $.post(
                    "../droits/json/add_autorisation.json.php?_="+Date.now(),
                    {
                        nom_aut: $("#nom_aut").val(),
                        code_aut: $("#code_aut").val(),
                        desc_aut_admin: $("#desc_aut_admin").val(),
                        desc_aut_user: $("#desc_aut_user").val()
                    },
                    function result(data, status){

                        message(data.message.texte, data.message.type);

                        if(data.response) {

                            $("#autorisations_form")[0].reset();
                        }

                    }
                );
            }
        }
    );
});
