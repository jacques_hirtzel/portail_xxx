<?php
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";
$aut = "ADM_AUT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
?>

<div class="row">
    <div class="header">
        <h1></h1>
    </div>
</div>

<div class="col-md-12">

    <div class="panel panel-primary">

        <div class="panel-heading">
            Ajouter un droit dans la base de donné
        </div>

        <div class="panel-body">
            <form id="autorisations_form" action="/droits/json/add_autorisation.json.php" method="post">

                <!-- Nom -->
                <div class="form-group row">
                    <label for="nom_aut" class="col-sm-2 col-form-label">Nom</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nom_aut" name="nom_aut" placeholder="Nom de l'autorisation">
                    </div>
                </div>

                <!-- Code -->
                <div class="form-group row">
                    <label for="code_aut" class="col-sm-2 col-form-label">Code de l'autorisation</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="code_aut" name="code_aut" placeholder="XXX">
                    </div>
                </div>

                <!-- Description administrateur -->
                <div class="form-group row">
                    <label for="desc_aut_admin" class="col-sm-2 col-form-label">Description de l'autorisation pour un administrateur</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="desc_aut_admin" name="desc_aut_admin" placeholder="Description"></textarea>
                    </div>
                </div>

                <!-- Description utilisateur -->
                <div class="form-group row">
                    <label for="desc_aut_user" class="col-sm-2 col-form-label">Description de l'autorisation pour un utilisateur</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="desc_aut_user" name="desc_aut_user" placeholder="Description"></textarea>
                    </div>
                </div>

                <!-- Groupe de boutton -->
                <div class="form-group action-button">
                    <input type="submit" class="btn btn-primary" value="Ajouter">
                    <a href="./autorisations.php" role="button" class="btn btn-warning">Annuler</a>
                </div>

            </form>
        </div>

        <div class="panel-footer">

        </div>

    </div>

</div>

</div>
<script src="./js/autorisations.js"></script>
</body>

</html>