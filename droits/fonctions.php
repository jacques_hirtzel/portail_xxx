<?php
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";
$aut = "ADM_AUT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
?>

<div class="row">
    <div class="header">
        <h1></h1>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            Ajouter un rôle dans la base de donné
        </div>

        <div class="panel-body">
            <form id="fonctions_form" action="/droits/json/add_fonction.json.php" method="post">

                <!-- Nom -->
                <div class="form-group row">
                    <label for="nom_fnc" class="col-sm-2 col-form-label">Nom</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nom_fnc" name="nom_fnc" placeholder="Nom de la fonction">
                    </div>
                </div>

                <!-- Abreviation -->
                <div class="form-group row">
                    <label for="abr_fnc" class="col-sm-2 col-form-label">Abreviation de la fonction</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="abr_fnc" name="abr_fnc" placeholder="Abreviation">
                    </div>
                </div>

                <!-- Description -->
                <div class="form-group row">
                    <label for="desc_fnc" class="col-sm-2 col-form-label">Description de la fonction</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="desc_fnc" name="desc_fnc" placeholder="Description"></textarea>
                    </div>
                </div>

                <!-- Groupe de boutton -->
                <div class="form-group action-button">
                    <input type="submit" class="btn btn-primary" value="Ajouter">
                    <a href="./fonctions.php" role="button" class="btn btn-warning">Annuler</a>
                </div>

            </form>
        </div>

        <div class="panel-footer">

        </div>

    </div>
</div>

</div>

<script src="./js/fonctions.js"></script>
</body>

</html>