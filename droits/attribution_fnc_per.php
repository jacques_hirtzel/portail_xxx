<?php
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";
$aut = "ADM_AUT";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
$fnc = new Fonction();
$tab_fnc = $fnc->get_all();

$tab_per_fnc = $fnc->get_tab_per_all_fnc();
?>
<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            Attribution des rôles aux utilisateurs
        </div>

        <div class="panel-body">
            <table class="table table-condensed table-bordered">
                <!--<thead>-->

                <thead>
                    <th>Nom de l'utilisateur</th>
                    <?php
                    foreach($tab_fnc as $key => $fonction) {
                        ?><th  class='text-center'>
                        <?php
                        echo $fonction['nom_fnc'];
                        echo "</th>";
                    }
                    ?>

                </thead>
                <!--</thead>-->
                <tbody>
                <?php
                $tab_per = $per->get_all();

                foreach($tab_per as $personne) {
                    echo "<tr>";
                    echo "<td>";
                    echo "<span>" . $personne['nom_per'] . " " . $personne['prenom_per'] . "</span>";
                    if($_SESSION['id'] != $personne['id_per']) {
                        echo '<button type="button" class="btn btn-danger pull-right btn_trash btn-xs" data-toggle="tooltip" data-placement="top" title="Supression" id_per="' . $personne['id_per'] . '"><i class="glyphicon glyphicon-trash"></i></button>'; // Doit être un administrateur pour utiliser le bouton de supression TODO <button type="submit" class="btn btn-disabled pull-right" data-toggle="tooltip" data-placement="top" title="Vous devez avoir les droits d'accès"><i class="glyphicon glyphicon-trash"></i></button>
                    }
                        echo "</td>";

                    foreach($tab_fnc as $fonction){
                        echo "<td class='text-center'>";
                        if($_SESSION['id'] != $personne['id_per'] || $fonction['id_fnc'] != 1) {
                            echo "<input type=\"checkbox\" title=\"".$fonction['nom_fnc']."\" class=\"auth\" id=\"auth_" . $fonction['id_fnc'] . "-" . $personne['id_per'] . "\" id_fnc=\"" . $fonction['id_fnc'] . "\" id_per=\"" . $personne['id_per'] . "\"";

                            if (isset($tab_per_fnc[$fonction['id_fnc']])) {
                                if (in_array($personne['id_per'], $tab_per_fnc[$fonction['id_fnc']])) {
                                    echo " checked=\"checked\"";
                                }
                            }
                            echo ">";
                        }
                        echo "</td>";
                    }

                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>

        <div class="panel-footer">

        </div>

    </div>
</div>

<script src="./js/attribution_fnc_per.js"></script>
