<?php
require_once substr(__dir__, 0, strpos(__dir__, "portail_xxx")+strlen("portail_xxx")) . "/config/config.inc.php";
require("./includes/head.inc.php");
?>

<div class="row">
    <div class="header">

    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            <h1>Portail_xxx</h1>

        </div>

        <div class="panel-body">

            <h4 class="space">Connexion</h4>

            <div class="clean"></div>

            <form id="login_form">

                <!-- Email -->
                <div class="form-group row justify-content-md-center">
                    <label for="email_per" class="col-sm-2 col-form-label">E-mail</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="email_per" name="email_per" placeholder="Votre adresse e-mail">
                    </div>
                </div>

                <!-- Mot de passe -->
                <div class="form-group row justify-content-md-center">
                    <label for="password" class="col-sm-2 col-form-label">Mot de passe</label>
                    <div class="col-sm-4">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Votre mot de passe">
                    </div>
                </div>
                <a href="./password_recovery.php" style="margin-left: 18%">J'ai oublié mon mot de passe</a>
                <!-- Groupe de boutton -->
                <div class="form-group action-button">
                    <input type="submit" class="btn btn-primary" value="Se connecter">
                    <a href="./index.php" role="button" class="btn btn-warning">Annuler</a>
                </div>

            </form>
        </div>

        <div class="panel-footer">
        </div>

    </div>
</div>

</div>
<script src="js/function.js"></script>
<script src="./js/login.js"></script>
</body>

</html>
